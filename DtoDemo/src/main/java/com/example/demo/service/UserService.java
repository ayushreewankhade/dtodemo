package com.example.demo.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.UserLocationDto;
import com.example.demo.model.User;
import com.example.demo.repos.UserRepo;

@Service
public class UserService {

	@Autowired
	private UserRepo userRepo;
	
	public List<UserLocationDto> getAllUserLocation(){
		return userRepo.findAll()
				.stream()
				.map(this::convertEntityToDto)
				.collect(Collectors.toList());
	}
	
	private UserLocationDto convertEntityToDto(User user) {
		UserLocationDto userLocationDto= new UserLocationDto();
		userLocationDto.setUserId(user.getId());
		userLocationDto.setEmail(user.getEmail());
		userLocationDto.setPlace(user.getLocation().getPlace());
		return userLocationDto;
	}
	
}
